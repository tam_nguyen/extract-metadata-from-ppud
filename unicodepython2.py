# -*- coding: utf-8 -*-
import csv
import unicodecsv
import os
import urllib

def append_row_to_csv(filename,row):
    #filehandler = open(filename, 'a')
    url = row[-1]
    filename = os.path.basename(url)
    row.append(urllib.unquote(filename))

    append_heading_row(filename,row)
    #csv_writer = unicodecsv.writer(filehandler,delimiter="|")
    #csv_writer.writerow(row)
    #filehandler.close()

def append_heading_row(filename,row):
    filehandler = open(filename, 'a')
    csv_writer = unicodecsv.writer(filehandler,delimiter="|")
    csv_writer.writerow(row)
    filehandler.close()
