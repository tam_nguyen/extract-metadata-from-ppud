#copy latest audio to folder
INPUT_FILE='/home/linuxuser/Music/kinh.txt'
KINH_FOLDER='/home/linuxuser/Music/kinh'
OUTPUT_FOLDER='/home/linuxuser/Music/kinhWorkingFolder'
url2string=""
main() {
	pushd "$KINH_FOLDER"
	while false && read data; do
	    #check size
	    foo=$(du -sh "$OUTPUT_FOLDER")
	    bar=${foo%%?/*}
	    
	    sizeClass=${bar: -1}
	    folderSize=${bar%[[:alpha:]]}
	    if [[ sizeClass = 'G' ]] && (( folderSize > 17 )); then
		break
	    fi
	    #echo $data
	    data=${data##*/}
	    if [ -e "$OUTPUT_FOLDER/$data" ]; then
		echo "skip existence file: $data"
	    elif [[ $data = *'???'* ]]; then
		continue
	    elif [[ $data = *.mp3 ]]; then	   
		urldecode $data
		cp --verbose "$url2string" "$OUTPUT_FOLDER"
	    elif [[ $data = *.mp4 ]]; then
		urldecode $data
		cp --verbose "$url2string" "$OUTPUT_FOLDER"
	    fi
	    #echo "$url2string"
	done < "$INPUT_FILE"
	popd
	
	pushd "$OUTPUT_FOLDER"
	for x in *.mp4 ; do
	    mp4Tomp3 "$x";
	done
	popd
}

#https://gist.github.com/cdown/1163649
urldecode() {
    local url_encoded="${1//+/ }"
    url2string=$(printf '%b' "${url_encoded//%/\x}")
    #echo $url_encoded "\n"
}

mp4Tomp3(){
    echo "input: " $1
    ffmpeg -i "$1" -b:a 192K -vn "${1/mp4/mp3}"
}

main
# use this also
#normalize-audio *.mp3