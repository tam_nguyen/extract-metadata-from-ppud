#id3v2
from csv import DictReader
from glob import glob
from subprocess import call

def get_metadata_collection(filename):
    file_handler = open(filename,'r')
    reader = DictReader(file_handler,delimiter='|')
    metadata_collection = {}
    for row in reader:
        metadata_collection[row["File Name"]] = row
    file_handler.close();
    return metadata_collection

def attach_metadata_to_files(metadata_collection):
    for mp3_file in glob("*.mp3"):
        try:
            metadata = metadata_collection[mp3_file]
            call(create_id3v2_metadata_string(metadata))
        except:
            print("File not in metadata_collection:", mp3_file)

def create_id3tool_metadata_string(metadata):
    result = ["id3tool","-r", metadata["Artist"], "-a",metadata["Album Name"], "-t",metadata["Title"],"-n",metadata["Description"],"-g", "12", "-y",metadata["Year"], "-c",metadata["Track Number"], metadata["File Name"]]

def create_id3v2_metadata_string(metadata):
    result = ["id3v2","-a", metadata["Artist"].encode("utf-8"), "-A",metadata["Album Name"].encode("utf-8"), "-t",metadata["Title"].encode("utf-8"),"-c",metadata["Description"].encode("utf-8"),"-g", "12", "-y",metadata["Year"], "-T",metadata["Track Number"], metadata["File Name"]]
    return result

def main():
    metadata_collection = get_metadata_collection('metadatatable.csv')
    attach_metadata_to_files(metadata_collection)

main()
